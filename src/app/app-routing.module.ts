import {NgModule} from '@angular/core';
import {Routes,RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ProjectsComponent } from './projects/projects.component';
import { WhyTeamYogiComponent } from './why-team-yogi/why-team-yogi.component';
import { ServiesComponent } from './servies/servies.component';
import { ContactComponent } from './contact/contact.component';





const routes: Routes =[
    { path:'',redirectTo:'\home',pathMatch:'full'},
    { path:'home',component:HomeComponent},
    { path:'aboutus',component:AboutUsComponent},
    { path:'projects',component:ProjectsComponent},
    { path:'why-Team',component:WhyTeamYogiComponent},
    { path:'servies',component:ServiesComponent},
    { path:'contactus',component:ContactComponent}
    


  
];

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})
export class AppRoutingModule{}
export const routingComponents=[HomeComponent]