import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhyTeamYogiComponent } from './why-team-yogi.component';

describe('WhyTeamYogiComponent', () => {
  let component: WhyTeamYogiComponent;
  let fixture: ComponentFixture<WhyTeamYogiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhyTeamYogiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhyTeamYogiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
